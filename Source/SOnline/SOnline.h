// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "SOnlineTypes.h"

#define SONLINE_DEBUG_VIEW // ���������� ��� �������� ������

#define CONCRETE_SURFACE SurfaceType1
#define METAL_SURFACE SurfaceType2
#define GLASS_SURFACE SurfaceType3
#define WOOD_SURFACE SurfaceType4
#define MEAT_SURFACE SurfaceType5