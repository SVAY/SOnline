// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "SOnline.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SOnline, "SOnline" );
