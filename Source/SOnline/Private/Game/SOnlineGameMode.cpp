// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "SOnlineGameMode.h"
#include "SOnlineCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "SPlayerController.h"
#include "SGettersLib.h"
#include "SOnlineGameState.h"

ASOnlineGameMode::ASOnlineGameMode()
{

}


void ASOnlineGameMode::PostLogin(APlayerController * NewPlayer)
{
	Super::PostLogin(NewPlayer);

	OnPlayerConnected(NewPlayer);
}

void ASOnlineGameMode::OnPlayerConnected(APlayerController* Controller)
{
	if (!Controller) { return; }

	USGettersLib::SGetSOnlineGameState(this)->NewPlayerConnected(Controller);
}



