// Fill out your copyright notice in the Description page of Project Settings.

#include "SGameModeBase.h"
#include "SGettersLib.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerState.h"
#include "GameFramework/GameStateBase.h"
#include "GameFramework/PlayerController.h"
#include "SPlayerStart.h"
#include "SPlayerState.h"
#include "SPlayerController.h"
#include "SGameStateBase.h"
#include "SSpectator.h"
#include "SGameInstance.h"

ASGameModeBase::ASGameModeBase()
{
	bStartPlayersAsSpectators = true;

	LimitByTime = true;
	TimeLimitInSeconds = 120;
	PlayerRespawnTime = 5;
	TimeToRemoveBodies = 20;
	AutoRespawn = true;
	AllowJoinInProgress = true;
}

void ASGameModeBase::StartGame()
{
	SpawnAllPlayers();
}

void ASGameModeBase::StopGame(FString Reason)
{
	AllPlayersToSpectators(false);
}

void ASGameModeBase::EndGame()
{
	USGettersLib::SGetGameInstance(this)->ServerTravelToLobby();
}

void ASGameModeBase::TimeLimitReached()
{
	StopGame("TimeReached");
}

bool ASGameModeBase::ShouldDamagePlayer(APlayerController* DamagedPlayer, APlayerController* DamageInstigator)
{
	return true;
}

void ASGameModeBase::OnPlayerConnected(APlayerController* Controller)
{
	Super::OnPlayerConnected(Controller);

	EGameState CurrentGameState = USGettersLib::SGetCurrentGameState(this);

	switch (CurrentGameState)
	{
	case EGameState::Wait:
		break;
	case EGameState::Game:
		if (AllowJoinInProgress)
		{
			SpawnPlayer(Controller);
		}
		break;
	case EGameState::GameEnd:
		break;
	default:
		break;
	}
}

void ASGameModeBase::SpawnPlayer(APlayerController* Controller)
{
	if (USGettersLib::SGetCurrentGameState(this) == EGameState::Game)
	{
		if (Controller->GetPawn())
		{
			Controller->GetPawn()->Destroy();
		}

		int NumSpawnPoints = GetFreeSpawnPoints(Controller).Num();

		if (NumSpawnPoints >= 1)
		{
			GetFreeSpawnPoints(Controller)[FMath::RandRange(0, NumSpawnPoints - 1)]->SpawnCharacter(Controller);

			USGettersLib::SGetPlayerState(Controller)->SetAlivePlayer(true);
		}
	}
}

void ASGameModeBase::PlayerDead(APlayerController* DeadPlayer, APlayerController* DeathInstigator, AActor* DeathCauser)
{
	USGettersLib::SGetPlayerState(DeadPlayer)->SetAlivePlayer(false);

	UpdateDeathPoints(DeadPlayer, DeathInstigator);

	if (DeathInstigator)
	{
		USGettersLib::SGetGameStateBase(this)->NotificationPlayerDead(DeadPlayer->PlayerState, DeathInstigator->PlayerState, DeathCauser);
	}
	else
	{
		USGettersLib::SGetGameStateBase(this)->NotificationPlayerDead(DeadPlayer->PlayerState, nullptr, DeathCauser);
	}

	if (AutoRespawn)
	{
		DeadPlayer->GetPawn()->SetLifeSpan(TimeToRemoveBodies);

		if (USGettersLib::SGetCurrentGameState(this) == EGameState::Game)
		{
			USGettersLib::SGetController(DeadPlayer)->LaunchRespawnTimer(PlayerRespawnTime);
		}
	}

	SpawnSpectator(DeadPlayer, false);
}

void ASGameModeBase::UpdateDeathPoints(APlayerController* DeadPlayer, APlayerController* DeathInstigator)
{
	if (DeathInstigator)
	{
		USGettersLib::SGetPlayerState(DeadPlayer)->AddDeath();
		USGettersLib::SGetPlayerState(DeathInstigator)->AddKill(1);
	}
	else
	{
		USGettersLib::SGetPlayerState(DeadPlayer)->AddDeath();
	}
}

void ASGameModeBase::SpawnAllPlayers()
{
	for (APlayerState* Player : GetWorld()->GetGameState()->PlayerArray)
	{
		APlayerController* Controller = Cast<APlayerController>(Player->GetOwner());
		SpawnPlayer(Controller);
	}
}

void ASGameModeBase::SpawnSpectator(APlayerController* Player, bool DeleteCurrentPawn)
{
	if (!Player) { return; }

	if (DeleteCurrentPawn)
	{
		if (Player->GetPawn())
		{
			Player->GetPawn()->Destroy();
		}
	}

	FActorSpawnParameters SpawnInfo;

	SpawnInfo.Owner = Player;

	ASSpectator* SpectatorSpawned = GetWorld()->SpawnActor<ASSpectator>(Player->GetPawn()->GetActorLocation(), Player->GetPawn()->GetActorRotation(), SpawnInfo);

	Player->Possess(SpectatorSpawned);
}

void ASGameModeBase::AllPlayersToSpectators(bool DeletePawns)
{
	TArray<AActor*> ActorControllers;

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerController::StaticClass(), ActorControllers);

	for (AActor* ItController : ActorControllers)
	{
		APlayerController* TempController = Cast<APlayerController>(ItController);
		SpawnSpectator(TempController, DeletePawns);
	}
}

TArray<ASPlayerStart*> ASGameModeBase::GetFreeSpawnPoints(APlayerController* Controller)
{
	TArray<AActor*> ActorPlayerStarts;

	TArray<ASPlayerStart*> FreeSpawnPoints;

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASPlayerStart::StaticClass(), ActorPlayerStarts);

	for (AActor* PlayerStart : ActorPlayerStarts)
	{
		ASPlayerStart* TempPlayerStart = Cast<ASPlayerStart>(PlayerStart);
		if (TempPlayerStart)
		{
			if (TempPlayerStart->IsFree())
			{
				FreeSpawnPoints.Add(TempPlayerStart);
			}
		}
	}

	return FreeSpawnPoints;
}

bool ASGameModeBase::GetLimitByTime()
{
	return LimitByTime;
}

int ASGameModeBase::GetTimeLimit()
{
	return TimeLimitInSeconds;
}
