// Fill out your copyright notice in the Description page of Project Settings.

#include "SPlayerStart.h"
#include "SOnlineCharacter.h"
#include "SGettersLib.h"
#include "Components/CapsuleComponent.h"
#include "SGameModeBase.h"

void ASPlayerStart::BeginPlay()
{
	IsFreePoint = true;

	Super::BeginPlay();

	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECR_Overlap);

	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &ASPlayerStart::StartOverlap);
	GetCapsuleComponent()->OnComponentEndOverlap.AddDynamic(this, &ASPlayerStart::EndOverlap);
}

ASOnlineCharacter* ASPlayerStart::SpawnCharacter(APlayerController* Controller)
{
	FActorSpawnParameters SpawnInfo;

	SpawnInfo.Owner = Controller;

	TSubclassOf <APawn> CharacterClass = USGettersLib::SGetGameModeBase(this)->DefaultPawnClass;

	APawn* SpawnedPawn = GetWorld()->SpawnActor<APawn>(CharacterClass, this->GetActorLocation(), this->GetActorRotation(), SpawnInfo);

	Controller->Possess(SpawnedPawn);

	return Cast<ASOnlineCharacter>(SpawnedPawn);
}

bool ASPlayerStart::IsFree()
{
	return IsFreePoint;
}

void ASPlayerStart::StartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	IsFreePoint = false;
}


void ASPlayerStart::EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	IsFreePoint = true;
}