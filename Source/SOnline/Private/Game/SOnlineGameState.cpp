// Fill out your copyright notice in the Description page of Project Settings.

#include "SOnlineGameState.h"
#include "SOnline.h"
#include "TimerManager.h"
#include "Net/UnrealNetwork.h"
#include "SGettersLib.h"
#include "SPlayerState.h"
#include "SGameModeBase.h"

ASOnlineGameState::ASOnlineGameState()
{
	GameState = EGameState::Wait;
}

void ASOnlineGameState::BeginPlay()
{
	Super::BeginPlay();

	if (Role == ROLE_Authority)
	{
		GetWorldTimerManager().SetTimer(StateTimeTick, this, &ASOnlineGameState::GameStateTick, 1.0f, true);
	}
}


void ASOnlineGameState::GameStateTick() // ������ EventTick ���������� �������� ������ �������
{
	
}

void ASOnlineGameState::GameLogicTick()
{

}

bool ASOnlineGameState::AllPlayersReady()
{
	for (APlayerState* It : PlayerArray)
	{
		ASPlayerState* PlayerState = Cast<ASPlayerState>(It);
		if (!PlayerState->ReadyForGame)
		{
			return false;
		}
	}

	return true;
}

void ASOnlineGameState::SetNewGameState_Implementation(EGameState NewState)
{
	GameState = NewState;

	NewGameState.Broadcast(GameState);
}

void ASOnlineGameState::NewPlayerConnected(APlayerController* Controller)
{
	PlayerConnected.Broadcast(Controller);
}

void ASOnlineGameState::PlayerReady(APlayerState* PlayerState)
{

}

TArray<ASPlayerState*> ASOnlineGameState::GetPlayers()
{
	TArray<ASPlayerState*> ReturnArray;

	for (APlayerState* ItState : PlayerArray)
	{
		ASPlayerState* AddState = Cast<ASPlayerState>(ItState);
		if (AddState)
		{
			ReturnArray.Add(AddState);
		}
	}

	return ReturnArray;
}

bool ASOnlineGameState::EnoughPlayersToStart()
{
	return false;
}

void ASOnlineGameState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASOnlineGameState, GameState);
}

//Getters


