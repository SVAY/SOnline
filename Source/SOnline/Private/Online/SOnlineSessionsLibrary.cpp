// Fill out your copyright notice in the Description page of Project Settings.

#include "SOnlineSessionsLibrary.h"
#include "Engine/LocalPlayer.h"

#define SESSION_LIB_DEBUG

FSUniqueNetId USOnlineSessionsLibrary::GetUniqueNetID(APlayerController *PlayerController)
{
	if (!PlayerController)
	{
		FSUniqueNetId NullID;
#ifdef SESSION_LIB_DEBUG
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("USSLib-GetUniqueNetID-Invalid PlayerController")));
#endif
		return NullID;
	}

	if (APlayerState* PlayerState = (PlayerController != NULL) ? PlayerController->PlayerState : NULL)
	{
		FSUniqueNetId ReturnID;

		ReturnID.SetUniqueNetId(PlayerState->UniqueId.GetUniqueNetId());

		if (ReturnID.IsValid())
		{
			return ReturnID;
		}
		else
		{
#ifdef SESSION_LIB_DEBUG
			GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("USSLib-GetUniqueNetID-Invalid ReturnID")));
#endif
		}	
	}

#ifdef SESSION_LIB_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("USSLib-GetUniqueNetID-Invalid PlayerState")));
#endif

	FSUniqueNetId NullID;
	return NullID;
}

FSUniqueNetId USOnlineSessionsLibrary::GetUniqueNetIDByPlayerState(APlayerState* PlayerState)
{
	if (!PlayerState)
	{
		FSUniqueNetId NullID;
		return NullID;
	}

	FSUniqueNetId ReturnID;

	ReturnID.SetUniqueNetId(PlayerState->UniqueId.GetUniqueNetId());

	return ReturnID;
}

FSFriendInfo USOnlineSessionsLibrary::GetFriend(APlayerController* PlayerController, FSUniqueNetId FriendID)
{
	if (!PlayerController)
	{
		FSFriendInfo NullInfo;
#ifdef SESSION_LIB_DEBUG
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("USSLib-GetFriend-Invalid PlayerController")));
#endif
		return NullInfo;
	}

	IOnlineFriendsPtr FriendsInterface = Online::GetFriendsInterface();

	if (!FriendsInterface.IsValid())
	{
#ifdef SESSION_LIB_DEBUG
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("USSLib-GetFriend-Invalid FriendsInterface")));
#endif
		FSFriendInfo NullInfo;
		return NullInfo;
	}

	ULocalPlayer* Player = Cast<ULocalPlayer>(PlayerController->Player);

	if (!Player)
	{
#ifdef SESSION_LIB_DEBUG
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("USSLib-GetFriend-Invalid LocalPlayer")));
#endif

		FSFriendInfo NullInfo;
		return NullInfo;
	}

	TSharedPtr<FOnlineFriend> Friend = FriendsInterface->GetFriend(Player->GetControllerId(), *FriendID.GetUniqueNetId(), EFriendsLists::ToString(EFriendsLists::Default));
	if (Friend.IsValid())
	{
		FSFriendInfo ReturnInfoFriend;

		FOnlineUserPresence Pres = Friend->GetPresence();
		
		ReturnInfoFriend.DisplayName = Friend->GetDisplayName();
		ReturnInfoFriend.RealName = Friend->GetRealName();
		ReturnInfoFriend.UniqueNetId.SetUniqueNetId(Friend->GetUserId());

		ReturnInfoFriend.PresenceInfo.bHasVoiceSupport = Pres.bHasVoiceSupport;
		ReturnInfoFriend.PresenceInfo.bIsJoinable = Pres.bIsJoinable;
		ReturnInfoFriend.PresenceInfo.bIsOnline = Pres.bIsOnline;
		ReturnInfoFriend.PresenceInfo.bIsPlayingThisGame = Pres.bIsPlayingThisGame;
		ReturnInfoFriend.PresenceInfo.PresenceState = ((ESOnlinePresenceState)((int32)Pres.Status.State));
		ReturnInfoFriend.PresenceInfo.StatusString = Pres.Status.StatusStr;

		return ReturnInfoFriend;
	}

#ifdef SESSION_LIB_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("USSLib-GetFriend-Invalid Friend")));
#endif

	FSFriendInfo NullInfo;
	return NullInfo;
}

void USOnlineSessionsLibrary::GetExtraSettings(FBlueprintSessionResult SessionResult, TArray<FSSessionPropertyKeyPair> & ExtraSettings)
{
	FSSessionPropertyKeyPair NewSetting;
	for (auto& Elem : SessionResult.OnlineResult.Session.SessionSettings.Settings)
	{
		NewSetting.Key = Elem.Key;
		NewSetting.Data = Elem.Value.Data;
		ExtraSettings.Add(NewSetting);
	}
}

FString USOnlineSessionsLibrary::GetPlayerNickname(const FSUniqueNetId &UniqueNetID)
{
	if (!UniqueNetID.IsValid())
	{
#ifdef SESSION_LIB_DEBUG
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("USSLib-GetNickName-Invalid UniqueNetID")));
#endif
		return FString(TEXT("NONE"));
	}

	IOnlineIdentityPtr IdentityInterface = Online::GetIdentityInterface();

	if (!IdentityInterface.IsValid())
	{
#ifdef SESSION_LIB_DEBUG
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("USSLib-GetNickName-Invalid IdentityInterface")));
#endif
		return FString(TEXT("NONE"));
	}

	return IdentityInterface->GetPlayerNickname(*UniqueNetID.GetUniqueNetId());
}

FSSessionPropertyKeyPair USOnlineSessionsLibrary::MakeLiteralSessionPropertyByte(FName Key, uint8 Value)
{
	FSSessionPropertyKeyPair Prop;
	Prop.Key = Key;
	Prop.Data.SetValue((int32)Value);
	return Prop;
}

FSSessionPropertyKeyPair USOnlineSessionsLibrary::MakeLiteralSessionPropertyBool(FName Key, bool Value)
{
	FSSessionPropertyKeyPair Prop;
	Prop.Key = Key;
	Prop.Data.SetValue(Value);
	return Prop;
}

FSSessionPropertyKeyPair USOnlineSessionsLibrary::MakeLiteralSessionPropertyString(FName Key, FString Value)
{
	FSSessionPropertyKeyPair Prop;
	Prop.Key = Key;
	Prop.Data.SetValue(Value);
	return Prop;
}

FSSessionPropertyKeyPair USOnlineSessionsLibrary::MakeLiteralSessionPropertyInt(FName Key, int32 Value)
{
	FSSessionPropertyKeyPair Prop;
	Prop.Key = Key;
	Prop.Data.SetValue(Value);
	return Prop;
}

FSSessionPropertyKeyPair USOnlineSessionsLibrary::MakeLiteralSessionPropertyFloat(FName Key, float Value)
{
	FSSessionPropertyKeyPair Prop;
	Prop.Key = Key;
	Prop.Data.SetValue(Value);
	return Prop;
}

void USOnlineSessionsLibrary::GetSessionPropertyByte(const TArray<FSSessionPropertyKeyPair> & ExtraSettings, FName SettingName, ESessionSettingSearchResult &SearchResult, uint8 &SettingValue)
{
	for (FSSessionPropertyKeyPair itr : ExtraSettings)
	{
		if (itr.Key == SettingName)
		{
			if (itr.Data.GetType() == EOnlineKeyValuePairDataType::Int32)
			{
				int32 Val;
				itr.Data.GetValue(Val);
				SettingValue = (uint8)(Val);
				SearchResult = ESessionSettingSearchResult::Found;
			}
			else
			{
				SearchResult = ESessionSettingSearchResult::WrongType;
			}
			return;
		}
	}

	SearchResult = ESessionSettingSearchResult::NotFound;
	return;
}

void USOnlineSessionsLibrary::GetSessionPropertyBool(const TArray<FSSessionPropertyKeyPair> & ExtraSettings, FName SettingName, ESessionSettingSearchResult &SearchResult, bool &SettingValue)
{
	for (FSSessionPropertyKeyPair itr : ExtraSettings)
	{
		if (itr.Key == SettingName)
		{
			if (itr.Data.GetType() == EOnlineKeyValuePairDataType::Bool)
			{
				itr.Data.GetValue(SettingValue);
				SearchResult = ESessionSettingSearchResult::Found;
			}
			else
			{
				SearchResult = ESessionSettingSearchResult::WrongType;
			}
			return;
		}
	}

	SearchResult = ESessionSettingSearchResult::NotFound;
	return;
}

void USOnlineSessionsLibrary::GetSessionPropertyString(const TArray<FSSessionPropertyKeyPair> & ExtraSettings, FName SettingName, ESessionSettingSearchResult &SearchResult, FString &SettingValue)
{
	for (FSSessionPropertyKeyPair itr : ExtraSettings)
	{
		if (itr.Key == SettingName)
		{
			if (itr.Data.GetType() == EOnlineKeyValuePairDataType::String)
			{
				itr.Data.GetValue(SettingValue);
				SearchResult = ESessionSettingSearchResult::Found;
			}
			else
			{
				SearchResult = ESessionSettingSearchResult::WrongType;
			}
			return;
		}
	}

	SearchResult = ESessionSettingSearchResult::NotFound;
	return;
}

void USOnlineSessionsLibrary::GetSessionPropertyInt(const TArray<FSSessionPropertyKeyPair> & ExtraSettings, FName SettingName, ESessionSettingSearchResult &SearchResult, int32 &SettingValue)
{
	for (FSSessionPropertyKeyPair itr : ExtraSettings)
	{
		if (itr.Key == SettingName)
		{
			if (itr.Data.GetType() == EOnlineKeyValuePairDataType::Int32)
			{
				itr.Data.GetValue(SettingValue);
				SearchResult = ESessionSettingSearchResult::Found;
			}
			else
			{
				SearchResult = ESessionSettingSearchResult::WrongType;
			}
			return;
		}
	}

	SearchResult = ESessionSettingSearchResult::NotFound;
	return;
}

void USOnlineSessionsLibrary::GetSessionPropertyFloat(const TArray<FSSessionPropertyKeyPair> & ExtraSettings, FName SettingName, ESessionSettingSearchResult &SearchResult, float &SettingValue)
{
	for (FSSessionPropertyKeyPair itr : ExtraSettings)
	{
		if (itr.Key == SettingName)
		{
			if (itr.Data.GetType() == EOnlineKeyValuePairDataType::Float)
			{
				itr.Data.GetValue(SettingValue);
				SearchResult = ESessionSettingSearchResult::Found;
			}
			else
			{
				SearchResult = ESessionSettingSearchResult::WrongType;
			}
			return;
		}
	}

	SearchResult = ESessionSettingSearchResult::NotFound;
	return;
}

// ===== STEAM ===== //

UTexture2D * USOnlineSessionsLibrary::GetSteamFriendAvatar(const FSUniqueNetId UniqueNetId, EBlueprintAsyncResultSwitch &Result, ESteamAvatarSize AvatarSize)
{
#if PLATFORM_WINDOWS || PLATFORM_MAC || PLATFORM_LINUX
	if (!UniqueNetId.IsValid() || !UniqueNetId.UniqueNetId->IsValid())
	{
		Result = EBlueprintAsyncResultSwitch::OnFailure;
		return nullptr;
	}

	uint32 Width = 0;
	uint32 Height = 0;

	if (SteamAPI_Init())
	{
		//Getting the PictureID from the SteamAPI and getting the Size with the ID
		//virtual bool RequestUserInformation( CSteamID steamIDUser, bool bRequireNameOnly ) = 0;


		uint64 id = *((uint64*)UniqueNetId.UniqueNetId->GetBytes());
		int Picture = 0;

		switch (AvatarSize)
		{
		case ESteamAvatarSize::SteamAvatar_Small: Picture = SteamFriends()->GetSmallFriendAvatar(id); break;
		case ESteamAvatarSize::SteamAvatar_Medium: Picture = SteamFriends()->GetMediumFriendAvatar(id); break;
		case ESteamAvatarSize::SteamAvatar_Large: Picture = SteamFriends()->GetLargeFriendAvatar(id); break;
		default: break;
		}

		if (Picture == -1)
		{
			Result = EBlueprintAsyncResultSwitch::AsyncLoading;
			return NULL;
		}

		SteamUtils()->GetImageSize(Picture, &Width, &Height);

		if (Width > 0 && Height > 0)
		{
			//Creating the buffer "oAvatarRGBA" and then filling it with the RGBA Stream from the Steam Avatar
			uint8 *oAvatarRGBA = new uint8[Width * Height * 4];


			//Filling the buffer with the RGBA Stream from the Steam Avatar and creating a UTextur2D to parse the RGBA Steam in
			SteamUtils()->GetImageRGBA(Picture, (uint8*)oAvatarRGBA, 4 * Height * Width * sizeof(char));

			UTexture2D* Avatar = UTexture2D::CreateTransient(Width, Height, PF_R8G8B8A8);
			// Switched to a Memcpy instead of byte by byte transer
			uint8* MipData = (uint8*)Avatar->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
			FMemory::Memcpy(MipData, (void*)oAvatarRGBA, Height * Width * 4);
			Avatar->PlatformData->Mips[0].BulkData.Unlock();

			// Original implementation was missing this!!
			// the hell man......
			delete[] oAvatarRGBA;

			//Setting some Parameters for the Texture and finally returning it
			Avatar->PlatformData->NumSlices = 1;
			Avatar->NeverStream = true;
			//Avatar->CompressionSettings = TC_EditorIcon;

			Avatar->UpdateResource();

			Result = EBlueprintAsyncResultSwitch::OnSuccess;
			return Avatar;
		}

		Result = EBlueprintAsyncResultSwitch::OnFailure;
		return nullptr;
	}
#endif

	Result = EBlueprintAsyncResultSwitch::OnFailure;
	return nullptr;
}
