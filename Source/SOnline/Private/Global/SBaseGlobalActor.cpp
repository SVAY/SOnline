// Fill out your copyright notice in the Description page of Project Settings.

#include "SBaseGlobalActor.h"
#include "SOnlineCharacter.h"


// Sets default values
ASBaseGlobalActor::ASBaseGlobalActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASBaseGlobalActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASBaseGlobalActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASBaseGlobalActor::Interact(ASOnlineCharacter* InteractPlayer)
{
	BlueprintIteractRewritable(InteractPlayer);
}

void ASBaseGlobalActor::BlueprintIteractRewritable_Implementation(ASOnlineCharacter* InteractPlayer)
{

}

