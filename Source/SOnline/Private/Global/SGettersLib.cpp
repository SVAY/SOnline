// Fill out your copyright notice in the Description page of Project Settings.

#include "SGettersLib.h"
#include "SOnlineGameState.h"
#include "SPlayerController.h"
#include "SPlayerState.h"
#include "SOnlineCharacter.h"
#include "SOnlineGameMode.h"
#include "SGameModeBase.h"
#include "SGameInstance.h"
#include "SGameStateBase.h"

USGameInstance* USGettersLib::SGetGameInstance(AActor* ActorForGetWorld)
{
	return ActorForGetWorld->GetWorld()->GetGameInstance<USGameInstance>();
}

ASOnlineGameMode* USGettersLib::SGetSonlineGameMode(AActor* ActorForGetWorld)
{
	return Cast<ASOnlineGameMode>(ActorForGetWorld->GetWorld()->GetAuthGameMode());
}

ASGameModeBase* USGettersLib::SGetGameModeBase(AActor* ActorForGetWorld)
{
	return Cast<ASGameModeBase>(ActorForGetWorld->GetWorld()->GetAuthGameMode());
}

ASOnlineGameState* USGettersLib::SGetSOnlineGameState(AActor* ActorForGetWorld)
{
	return Cast<ASOnlineGameState>(ActorForGetWorld->GetWorld()->GetGameState());
}

ASGameStateBase* USGettersLib::SGetGameStateBase(AActor* ActorForGetWorld)
{
	return Cast<ASGameStateBase>(ActorForGetWorld->GetWorld()->GetGameState());
}

ASPlayerState* USGettersLib::SGetPlayerState(APlayerController* Contoller)
{
	return Cast<ASPlayerState>(Contoller->PlayerState);
}

ASPlayerState* USGettersLib::SGetPlayerState(ASOnlineCharacter* Character)
{
	return Cast<ASPlayerState>(Character->Controller->PlayerState);
}

ASPlayerState* USGettersLib::SGetPlayerState(APlayerState* PlayerState)
{
	return Cast<ASPlayerState>(PlayerState);
}

ASPlayerState* USGettersLib::SGetPlayerState(AController* Contoller)
{
	return Cast<ASPlayerState>(Contoller->PlayerState);
}

ASPlayerController* USGettersLib::SGetController(AController* Controller)
{
	return Cast<ASPlayerController>(Controller);
}

ASPlayerController* USGettersLib::SGetController(ASOnlineCharacter* Character)
{
	return Cast<ASPlayerController>(Character->GetController());
}

APlayerController* USGettersLib::GetPlayerController(AController* Controller)
{
	return Cast<APlayerController>(Controller);
}

ASOnlineCharacter* USGettersLib::SGetCharacter(AController* Controller)
{
	return Cast<ASOnlineCharacter>(Controller->GetPawn());
}

ASOnlineCharacter* USGettersLib::SGetCharacter(APlayerState* PlayerState)
{
	return Cast<ASOnlineCharacter>(PlayerState->GetOwner());
}

ASOnlineCharacter* USGettersLib::SGetCharacter(ASPlayerController* Controller)
{
	return Cast<ASOnlineCharacter>(Controller->GetPawn());
}

EGameState USGettersLib::SGetCurrentGameState(AActor* ActorForGetWorld)
{
	return SGetSOnlineGameState(ActorForGetWorld)->GameState;
}

