// Fill out your copyright notice in the Description page of Project Settings.

#include "SGameInstance.h"
#include "SOnline.h"
#include "Online.h"
#include "OnlineSubsystem.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/GameMode.h"
#include "Kismet/GameplayStatics.h"

#define SESSIONS_DEBUG

USGameInstance::USGameInstance(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	/* ������� ���������� ��� �������� Session */
	OnCreateSessionCompleteDelegate = FOnCreateSessionCompleteDelegate::CreateUObject(this, &USGameInstance::OnCreateSessionComplete);
	OnStartSessionCompleteDelegate = FOnStartSessionCompleteDelegate::CreateUObject(this, &USGameInstance::OnStartOnlineSessionComplete);

	OnFindSessionsCompleteDelegate = FOnFindSessionsCompleteDelegate::CreateUObject(this, &USGameInstance::OnFindSessionsComplete);

	OnDestroySessionCompleteDelegate = FOnDestroySessionCompleteDelegate::CreateUObject(this, &USGameInstance::OnDestroySessionComplete);
}

void USGameInstance::Init()
{
	Super::Init();
}

bool USGameInstance::HostSession(FSUniqueNetId UserId,FSServerSettings HostSettings, FName SessionName)
{
	IOnlineSubsystem* const OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		IOnlineSessionPtr SessionInterface = OnlineSub->GetSessionInterface();
		
		if (SessionInterface.IsValid() && UserId.IsValid())
		{
			TSharedPtr<class FOnlineSessionSettings> SessionSettings;
			SessionSettings = MakeShareable(new FOnlineSessionSettings());

			SessionSettings->bIsLANMatch = HostSettings.bUseLAN;
			SessionSettings->bUsesPresence = HostSettings.bUsePresence;
			SessionSettings->NumPublicConnections = HostSettings.NumPublicConnections;
			SessionSettings->NumPrivateConnections = HostSettings.NumPrivateConnections;
			SessionSettings->bAllowInvites = HostSettings.bAllowInvites;
			SessionSettings->bAllowJoinInProgress = HostSettings.bAllowJoinInProgress;
			SessionSettings->bShouldAdvertise = HostSettings.bShouldAdvertise;
			SessionSettings->bAllowJoinViaPresence = HostSettings.bAllowJoinViaPresence;
			SessionSettings->bAllowJoinViaPresenceFriendsOnly = HostSettings.bAllowJoinViaPresenceFriendsOnly;

			if (HostSettings.bDedicatedServer) { SessionSettings->bUsesPresence = false; }
			else
			{ SessionSettings->bUsesPresence = HostSettings.bUsePresence; }


			FOnlineSessionSetting ExtraSetting;
			for (int i = 0; i < HostSettings.ExtraSettings.Num(); i++)
			{
				ExtraSetting.Data = HostSettings.ExtraSettings[i].Data;
				ExtraSetting.AdvertisementType = EOnlineDataAdvertisementType::ViaOnlineService;
				SessionSettings->Settings.Add(HostSettings.ExtraSettings[i].Key, ExtraSetting);
			}

			OnCreateSessionCompleteDelegateHandle = SessionInterface->AddOnCreateSessionCompleteDelegate_Handle(OnCreateSessionCompleteDelegate);

			return SessionInterface->CreateSession(*UserId.GetUniqueNetId(), SessionName, *SessionSettings);
		}
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 30.f, FColor::Red, TEXT("No OnlineSubsytem found!"));
	}

	return false;
}

void USGameInstance::FindSessions(FSUniqueNetId UserID, FSSearchSettings SearchSettings)
{
	IOnlineSubsystem* const OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		IOnlineSessionPtr SessionInterface = OnlineSub->GetSessionInterface();

		if (SessionInterface.IsValid() && UserID.IsValid())
		{
			bool bRunSecondSearch = false;

			TSharedPtr<FOnlineSessionSearch> SearchObjectDedicated;
			SessionsSearch = MakeShareable(new FOnlineSessionSearch());
			SessionsSearch->MaxSearchResults = SearchSettings.MaxResults;
			SessionsSearch->bIsLanQuery = SearchSettings.bIsLAN;

			// Store extra settings
			TArray<FSessionsSearchSetting> SearchExtraSettings = SearchSettings.Filters;

			// Create temp filter variable, because I had to re-define a blueprint version of this, it is required.
			FOnlineSearchSettingsEx Tem;

			if (SearchSettings.bEmptyServersOnly)
				Tem.Set(SEARCH_EMPTY_SERVERS_ONLY, true, EOnlineComparisonOp::Equals);

			if (SearchSettings.bNonEmptyServersOnly)
				Tem.Set(SEARCH_NONEMPTY_SERVERS_ONLY, true, EOnlineComparisonOp::Equals);

			if (SearchSettings.bSecureServersOnly)
				Tem.Set(SEARCH_SECURE_SERVERS_ONLY, true, EOnlineComparisonOp::Equals);

			if (SearchSettings.MinSlotsAvailable != 0)
				Tem.Set(SEARCH_MINSLOTSAVAILABLE, SearchSettings.MinSlotsAvailable, EOnlineComparisonOp::GreaterThanEquals);

			if (SearchSettings.UseFilters)
			{
				if (SearchExtraSettings.Num() > 0)
				{
					for (int i = 0; i < SearchExtraSettings.Num(); i++)
					{
						// Function that was added to make directly adding a FVariant possible

						Tem.HardSet(SearchExtraSettings[i].PropertyKeyPair.Key, SearchExtraSettings[i].PropertyKeyPair.Data, SearchExtraSettings[i].ComparisonOp);
					}
				}
			}

			switch (SearchSettings.ServerSearchType)
			{

				case EServerPresenceSearchType::ClientServersOnly: { Tem.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals); } break;
				case EServerPresenceSearchType::DedicatedServersOnly: {} break;
				case EServerPresenceSearchType::AllServers:
				default:
				{
					// Only steam uses the separate searching flags currently
					if (IOnlineSubsystem::DoesInstanceExist("STEAM"))
					{
						bRunSecondSearch = true;

						SearchObjectDedicated = MakeShareable(new FOnlineSessionSearch);
						SearchObjectDedicated->MaxSearchResults = SearchSettings.MaxResults;
						SearchObjectDedicated->bIsLanQuery = SearchSettings.bIsLAN;

						FOnlineSearchSettingsEx DedicatedOnly = Tem;
						Tem.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);

						//DedicatedOnly.Set(SEARCH_DEDICATED_ONLY, true, EOnlineComparisonOp::Equals);
						SearchObjectDedicated->QuerySettings = DedicatedOnly;
					}
				}
				break;
			}

			SessionsSearch->QuerySettings = Tem;

			OnFindSessionsCompleteDelegateHandle = SessionInterface->AddOnFindSessionsCompleteDelegate_Handle(OnFindSessionsCompleteDelegate);

			SessionInterface->FindSessions(*UserID.GetUniqueNetId(), SessionsSearch.ToSharedRef());

			return;
		}
	}
	else
	{
		OnFindSessionsComplete(false);
	}
}

void USGameInstance::DestroySessionAndLeaveGame()
{
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		IOnlineSessionPtr SessionInterface = OnlineSub->GetSessionInterface();
		if (SessionInterface.IsValid())
		{
			SessionInterface->AddOnDestroySessionCompleteDelegate_Handle(OnDestroySessionCompleteDelegate);

			SessionInterface->DestroySession(GameSessionName);
		}
	}
}

void USGameInstance::ServerTravelToLobby()
{
	GetWorld()->ServerTravel("/Game/Maps/LVL_Lobby?listen");
}

void USGameInstance::OnCreateSessionComplete(FName SessionName, bool bWasSuccessful)
{
#ifdef SESSIONS_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Green, FString::Printf(TEXT("OnSessionCreateComplete %s, %d"), *SessionName.ToString(), bWasSuccessful));
#endif
	IOnlineSubsystem* const OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		IOnlineSessionPtr SessionInterface = OnlineSub->GetSessionInterface();

		if (SessionInterface.IsValid())
		{
			SessionInterface->ClearOnCreateSessionCompleteDelegate_Handle(OnCreateSessionCompleteDelegateHandle);

			if (bWasSuccessful)
			{
				OnStartSessionCompleteDelegateHandle = SessionInterface->AddOnStartSessionCompleteDelegate_Handle(OnStartSessionCompleteDelegate);
				SessionInterface->StartSession(SessionName);
			}
		}
	}
}

void USGameInstance::OnStartOnlineSessionComplete(FName SessionName, bool bWasSuccessful)
{
#ifdef SESSIONS_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Green, FString::Printf(TEXT("OnStartSessionComplete %s, %d"), *SessionName.ToString(), bWasSuccessful));
#endif
	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		IOnlineSessionPtr SessionInterface = OnlineSub->GetSessionInterface();
		if (SessionInterface.IsValid())
		{
			SessionInterface->ClearOnStartSessionCompleteDelegate_Handle(OnStartSessionCompleteDelegateHandle);
		}
	}

	if (bWasSuccessful)
	{
		GetWorld()->ServerTravel("/Game/Maps/LVL_Lobby?listen");
	}
}

void USGameInstance::OnFindSessionsComplete(bool bWasSuccessful)
{
#ifdef SESSIONS_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Green, FString::Printf(TEXT("OFindSessionsComplete bSuccess: %d"), bWasSuccessful));
#endif

	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		IOnlineSessionPtr SessionInterface = OnlineSub->GetSessionInterface();
		if (SessionInterface.IsValid())
		{
			SessionInterface->ClearOnFindSessionsCompleteDelegate_Handle(OnFindSessionsCompleteDelegateHandle);

#ifdef SESSIONS_DEBUG
			GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Green, FString::Printf(TEXT("Num Search Results: %d"), SessionsSearch->SearchResults.Num()));
#endif // SONLINE_DEBUG_VIEW

			if (SessionsSearch->SearchResults.Num() > 0)
			{
				TArray<FBlueprintSessionResult> BluepruntSessionsOutput;

				for (auto& Result : SessionsSearch->SearchResults)
				{
					FBlueprintSessionResult BPResult;
					BPResult.OnlineResult = Result;
					BluepruntSessionsOutput.Add(BPResult);
				}

				SessionFindCompleteEvent.Broadcast(BluepruntSessionsOutput);
			}
		}
	}
}

void USGameInstance::OnDestroySessionComplete(FName SessionName, bool bWasSuccessful)
{
#ifdef SESSIONS_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("OnDestroySessionComplete %s, %d"), *SessionName.ToString(), bWasSuccessful));
#endif // SESSIONS_DEBUG

	IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub)
	{
		IOnlineSessionPtr SessionInterface = OnlineSub->GetSessionInterface();
		if (SessionInterface.IsValid())
		{
			SessionInterface->ClearOnDestroySessionCompleteDelegate_Handle(OnDestroySessionCompleteDelegateHandle);
			if (bWasSuccessful)
			{
				UGameplayStatics::OpenLevel(GetWorld(), "LVL_MainMenu", true);
			}
		}
	}
}