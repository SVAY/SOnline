// Fill out your copyright notice in the Description page of Project Settings.

#include "SHUD.h"
#include "SGettersLib.h"
#include "Blueprint/UserWidget.h"
#include "SOnlineGameState.h"

void ASHUD::BeginPlay()
{
	Super::BeginPlay();

	USGettersLib::SGetSOnlineGameState(this)->NewGameState.AddDynamic(this, &ASHUD::OnChangeGameState);
	OnChangeGameState(EGameState::Wait);
}

void ASHUD::OnChangeGameState(EGameState NewState)
{
	switch (NewState)
	{
	case EGameState::Wait:
		TooglePreStart(true);
		break;
	case EGameState::Game:
		TooglePreStart(false);
		ToogleHUD(true);
		break;
	case EGameState::GameEnd:
		break;
	default:
		break;
	}
}

void ASHUD::ToogleHUD(bool Show)
{
	if (HUD_WidgetClass)
	{
		if (Show)
		{
			if (HUD_Widget)
			{
				HUD_Widget->SetVisibility(ESlateVisibility::Visible);
			}
			else
			{
				HUD_Widget = CreateWidget<UUserWidget>(PlayerOwner, HUD_WidgetClass);
				HUD_Widget->AddToViewport();
			}
		}
		else
		{
			if (HUD_Widget)
			{
				HUD_Widget->RemoveFromParent();
			}
		}
	}	
}

void ASHUD::TooglePreStart(bool Show)
{
	if (PreStart_WidgetClass)
	{
		if (Show)
		{
			if (PreStart_Widget)
			{
				PreStart_Widget->SetVisibility(ESlateVisibility::Visible);
			}
			else
			{
				PreStart_Widget = CreateWidget<UUserWidget>(PlayerOwner, PreStart_WidgetClass);
				PreStart_Widget->AddToViewport();
			}
		}
		else
		{
			if (PreStart_Widget)
			{
				PreStart_Widget->RemoveFromParent();
			}
		}
	}
}
