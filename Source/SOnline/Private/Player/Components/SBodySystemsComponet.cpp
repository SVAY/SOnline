// Fill out your copyright notice in the Description page of Project Settings.

#include "SBodySystemsComponet.h"
#include "SOnlineCharacter.h"
#include "SGameModeBase.h"
#include "SGettersLib.h"
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
USBodySystemsComponet::USBodySystemsComponet()
{
	bReplicates = true;

	IsBodyOff = false;
	
	WalkSpeed = 600;
	SprintSpeed = 900;
	EnergyDecreaseInSprint = 0.1;

	Energy = 100;
	MaxEnergy = 100;

	EnableCoolingSystem = true;
	CurrentTemperature = 100;
	PowerCoolingSysytem = 0;
	MaxCoolingTemp = 2;
	CoolingSysMaxEnergyDecrease = 0.1;
	DefaultTemperatureRise = 0.01;

	SystemVoltage = 0.1;

	EnableShieldSystem = true;
	ShieldEffectivity = 100;
	ShieldRegenerationSpeed = 0.1;
	ShieldMaxEnergyDecrease = 0.2;
	ShieldMinEnergyDecrease = 0.01;
	ShieldAbsorptionEfficiency = 50;
	TimeWithoutDamageForRegen = 5;
}


// Called when the game starts
void USBodySystemsComponet::BeginPlay()
{
	Super::BeginPlay();

	OwnerComponent = Cast<ASOnlineCharacter>(GetOwner());
	
	if (OwnerComponent)
	{
		if (OwnerComponent->Role == ROLE_Authority)
		{
			OwnerComponent->OnTakeAnyDamage.AddDynamic(this, &USBodySystemsComponet::ApplyDamage);	

			GetWorld()->GetTimerManager().SetTimer(EnergyUpdateTimer, this, &USBodySystemsComponet::BodySystemsTick, 0.1, true);
		}
	}
}

void USBodySystemsComponet::ApplyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage <= 0 || IsBodyOff) { return; }

	APlayerController* DamagedPlayerController = Cast<APlayerController>(DamagedActor->GetInstigatorController());
	APlayerController* InstigatorPlayerController = USGettersLib::GetPlayerController(InstigatedBy);

	//if (!DamagedPlayerController || !InstigatorPlayerController) { return; }

	if (USGettersLib::SGetGameModeBase(DamagedActor)->ShouldDamagePlayer(DamagedPlayerController, InstigatorPlayerController))
	{
		LastDamageCauser = DamageCauser;

		if (EnableShieldSystem)
		{
			if (ShieldEffectivity - FMath::Lerp(Damage, (Damage*ShieldAbsorptionEfficiency) / 100, ShieldEffectivity / 100) < 0)
			{
				DamageModules(ShieldEffectivity - FMath::Lerp(Damage, (Damage*ShieldAbsorptionEfficiency) / 100, ShieldEffectivity / 100));
			}

			ShieldEffectivity = FMath::Clamp(ShieldEffectivity - FMath::Lerp(Damage, (Damage*ShieldAbsorptionEfficiency) / 100, ShieldEffectivity / 100), 0.f, 100.f);
			LastDamageTime = GetWorld()->TimeSeconds;
#ifdef BODYSYSTEMS_DEBUG
			GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("SHIELD EFFECTIVITY: - %f"), FMath::Lerp(Damage, (Damage*ShieldAbsorptionEfficiency) / 100, ShieldEffectivity / 100)));
#endif
		}
		else
		{
			DamageModules(Damage);
		}
	}
}

void USBodySystemsComponet::BodySystemsTick() // �alled 10 times per second
{
	TemperatureLogicTick();

	if (!IsBodyOff)
	{
		EnergyLogicTick();
		ShieldLogicTick();

		SystemVoltage = FMath::Lerp(50.f, 2.f, ShieldEffectivity / 100) + FMath::Lerp(0.1f, 5.f, PowerCoolingSysytem / 100) + UKismetMathLibrary::SelectFloat(EnergyDecreaseInSprint, 0.f, IsSprint);
	}
}

void USBodySystemsComponet::EnergyLogicTick()
{
	if (Energy > 0)
	{
		if (EnableCoolingSystem)
		{
			Energy -= FMath::Lerp(0.f, CoolingSysMaxEnergyDecrease, PowerCoolingSysytem / 100);
		}

		if (EnableShieldSystem)
		{
			Energy -= FMath::Lerp(ShieldMaxEnergyDecrease, ShieldMinEnergyDecrease, ShieldEffectivity / 100);
		}
	}
	else
	{
		EndOfEnergy();
	}
}

void USBodySystemsComponet::TemperatureLogicTick()
{
	CurrentTemperature += FMath::Lerp(DefaultTemperatureRise, 5.f, FMath::Clamp(SystemVoltage / 100, 0.f, 1.f));

	float CriticalTemperature = 300;

	if (CurrentTemperature > CriticalTemperature)
	{
		DamageModules(CurrentTemperature - CriticalTemperature);
	}

	if (EnableCoolingSystem)
	{
		PowerCoolingSysytem = FMath::Lerp(0.f, 100.f, FMath::GetMappedRangeValueClamped(FVector2D(100.f, 200.f), FVector2D(0.f, 1.f), CurrentTemperature));

		CurrentTemperature -= FMath::Lerp(0.f, MaxCoolingTemp, PowerCoolingSysytem / 100);
	}
}

void USBodySystemsComponet::ShieldLogicTick()
{
	if (ShieldEffectivity < 100 && GetWorld()->TimeSeconds - LastDamageTime > TimeWithoutDamageForRegen)
	{
		ShieldEffectivity = FMath::Clamp(ShieldEffectivity + ShieldRegenerationSpeed, 0.f, 100.f);
	}
}

void USBodySystemsComponet::DamageModules(float Damage)
{
#ifdef BODYSYSTEMS_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("DAMAGE MODULES: %f"), Damage));
#endif
	BodyShutdown();
}

void USBodySystemsComponet::EndOfEnergy()
{
#ifdef BODYSYSTEMS_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("ENERGY ENDED")));
#endif
	BodyShutdown();
}

void USBodySystemsComponet::BodyShutdown()
{
#ifdef BODYSYSTEMS_DEBUG
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("BodyShutdown")));
#endif

	IsBodyOff = true;
	SystemVoltage = 0;
	DefaultTemperatureRise = -0.1;
	EnableCoolingSystem = false;
	EnableShieldSystem = false;

	APlayerController* PC = Cast<APlayerController>(OwnerComponent->GetController());

	USGettersLib::SGetGameModeBase(OwnerComponent)->PlayerDead(PC, USGettersLib::GetPlayerController(OwnerComponent->LastHitBy), LastDamageCauser);
}

bool USBodySystemsComponet::SwitchSprint(bool Start_Stop)
{
	IsSprint = Start_Stop;

	if (IsSprint)
	{
		OwnerComponent->GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
	}
	else
	{
		OwnerComponent->GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
	}

	return IsSprint;
}

bool USBodySystemsComponet::SwitchShieldSystem()
{
	SwitchShieldSystemToServer();
	return !EnableShieldSystem;
}

bool USBodySystemsComponet::SwitchCoolingSystem()
{
	SwitchCoolingSystemToServer();
	return !EnableCoolingSystem;
}

void USBodySystemsComponet::SwitchShieldSystemToServer_Implementation()
{
	EnableShieldSystem = !EnableShieldSystem;
}

bool USBodySystemsComponet::SwitchShieldSystemToServer_Validate()
{
	return true;
}

void USBodySystemsComponet::SwitchCoolingSystemToServer_Implementation()
{
	EnableCoolingSystem = !EnableCoolingSystem;
}

bool USBodySystemsComponet::SwitchCoolingSystemToServer_Validate()
{
	return true;
}

void USBodySystemsComponet::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USBodySystemsComponet, IsBodyOff);
	DOREPLIFETIME_CONDITION(USBodySystemsComponet, Energy, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(USBodySystemsComponet, EnableShieldSystem, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(USBodySystemsComponet, ShieldEffectivity, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(USBodySystemsComponet, EnableCoolingSystem, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(USBodySystemsComponet, CurrentTemperature, COND_OwnerOnly);
}

//// GETTERS /////
