// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "SOnlineCharacter.h"
//#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/KismetMathLibrary.h"
#include "SOnline.h"
#include "SBaseGlobalActor.h"
#include "SWeaponManager.h"
#include "SBodySystemsComponet.h"
#include "SWeaponBase.h"
#include "SGameplayTypes.h"

#ifdef SONLINE_DEBUG_VIEW
#include "DrawDebugHelpers.h"
#endif


ASOnlineCharacter::ASOnlineCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;
	GetCharacterMovement()->CrouchedHalfHeight = 60;
	GetCharacterMovement()->MaxWalkSpeedCrouched = 200;
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
	GetCharacterMovement()->bCanWalkOffLedgesWhenCrouching = true;
	GetCharacterMovement()->GetNavAgentPropertiesRef().bCanCrouch = true;
	GetCharacterMovement()->NavAgentProps.bCanCrouch = true;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
	CameraBoom->bEnableCameraLag = true;

	WeaponManager = CreateDefaultSubobject<USWeaponManager>(TEXT("WeaponManager"));
	BodySystems = CreateDefaultSubobject<USBodySystemsComponet>(TEXT("BodySystems"));
}

void ASOnlineCharacter::BeginPlay()
{
	Super::BeginPlay();

}

FVector ASOnlineCharacter::GetCameraForwardVector()
{
	return FollowCamera->GetForwardVector();
}

FRotator ASOnlineCharacter::GetCameraRotation()
{
	return FollowCamera->GetComponentRotation();
}

void ASOnlineCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Role == ROLE_Authority)
	{
		if (GetController())
		{
			//FRotator TempRot = UKismetMathLibrary::NormalizedDeltaRotator(GetController()->GetControlRotation(), this->GetActorRotation());

			LookAngles = GetController()->GetControlRotation();
		}
	}

}

void ASOnlineCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ASOnlineCharacter::StartCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ASOnlineCharacter::StopCrouch);

	PlayerInputComponent->BindAction("UseWeapon", IE_Pressed, this, &ASOnlineCharacter::StartUseWeapon);
	PlayerInputComponent->BindAction("UseWeapon", IE_Released, this, &ASOnlineCharacter::StopUseWeapon);

	//PlayerInputComponent->BindAction("UseSecondaryAction", IE_Pressed, this, &ASOnlineCharacter::StartUseSecondaryAction);
	//PlayerInputComponent->BindAction("UseSecondaryAction", IE_Released, this, &ASOnlineCharacter::StopUseSecondaryAction);

	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ASOnlineCharacter::ServerTraceInterract);

	//PlayerInputComponent->BindAction("DropWeapon", IE_Pressed, this, &ASOnlineCharacter::DropWeapon);
	//PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &ASOnlineCharacter::StartReload);

	PlayerInputComponent->BindAction("SelectBasicSlot", IE_Pressed, this, &ASOnlineCharacter::SelectBasicSlot);
	PlayerInputComponent->BindAction("SelectSecondarySlot", IE_Pressed, this, &ASOnlineCharacter::SelectSecondarySlot);
	PlayerInputComponent->BindAction("SelectAdditionalSlot", IE_Pressed, this, &ASOnlineCharacter::SelectAdditionalSlot);
	//PlayerInputComponent->BindAction("SelectNextSlot", IE_Pressed, this, &ASOnlineCharacter::SelectNextSlot);
	//PlayerInputComponent->BindAction("SelectPreviousSlot", IE_Pressed, this, &ASOnlineCharacter::SelectPreviousSlot);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ASOnlineCharacter::StartSprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ASOnlineCharacter::StopSprint);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASOnlineCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASOnlineCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ASOnlineCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ASOnlineCharacter::LookUpAtRate);
}

void ASOnlineCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ASOnlineCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ASOnlineCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ASOnlineCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ASOnlineCharacter::StartCrouch()
{
	Crouch();
}

void ASOnlineCharacter::StopCrouch()
{
	UnCrouch();
}

FVector ASOnlineCharacter::GetPawnViewLocation() const
{
	if (FollowCamera)
	{
		return FollowCamera->GetComponentLocation();
	}

	return Super::GetPawnViewLocation();
}

void ASOnlineCharacter::ServerTraceInterract_Implementation()
{
	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(this);
	if (WeaponManager->GetCurrentWeapon())
	{QueryParams.AddIgnoredActor(WeaponManager->GetCurrentWeapon());}
	FVector TraceStart;
	FRotator RotationTrace;
	FVector TraceEnd;
	GetActorEyesViewPoint(TraceStart, RotationTrace);

	TraceEnd = TraceStart + (RotationTrace.Vector() * 500);
	TraceStart = TraceStart + FollowCamera->GetForwardVector() * 200;

	FHitResult HitInteract;
	if (GetWorld()->LineTraceSingleByChannel(HitInteract, TraceStart, TraceEnd, ECC_Visibility, QueryParams))
	{
		ASBaseGlobalActor* CustomActorCheck = Cast<ASBaseGlobalActor>(HitInteract.GetActor());

		if (CustomActorCheck)
		{
			CustomActorCheck->Interact(this);
		}
	}

#ifdef SONLINE_DEBUG_VIEW
	DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Green, false, 1.0f, 0, 1.0f);
#endif
}

bool ASOnlineCharacter::ServerTraceInterract_Validate()
{
	return true;
}

void ASOnlineCharacter::StartUseWeapon()
{
	if (WeaponManager->GetCurrentWeapon())
	{
		WeaponManager->GetCurrentWeapon()->UseWeapon(true);
	}	
}

void ASOnlineCharacter::StopUseWeapon()
{
	if (WeaponManager->GetCurrentWeapon())
	{
		WeaponManager->GetCurrentWeapon()->UseWeapon(false);
	}
}

void ASOnlineCharacter::StartSprint()
{
	BodySystems->SwitchSprint(true);
}

void ASOnlineCharacter::StopSprint()
{
	BodySystems->SwitchSprint(false);
}

void ASOnlineCharacter::SelectBasicSlot()
{
	WeaponManager->SelectWeapon(ESlotType::Basic);
}

void ASOnlineCharacter::SelectSecondarySlot()
{
	WeaponManager->SelectWeapon(ESlotType::Secondary);
}

void ASOnlineCharacter::SelectAdditionalSlot()
{
	WeaponManager->SelectWeapon(ESlotType::None); // CHANGE ADDITIONAL - ONLY FOR TESTS
}

void ASOnlineCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASOnlineCharacter, LookAngles);
	DOREPLIFETIME(ASOnlineCharacter, WeaponManager);
	DOREPLIFETIME(ASOnlineCharacter, BodySystems);
}