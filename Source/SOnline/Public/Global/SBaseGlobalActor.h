// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SBaseGlobalActor.generated.h"

class ASOnlineCharacter;

UCLASS()
class SONLINE_API ASBaseGlobalActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASBaseGlobalActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(ASOnlineCharacter* InteractPlayer); //���������� ������� ��������������
	
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Interact")
	void BlueprintIteractRewritable(ASOnlineCharacter* InteractPlayer);
};
