// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "SGameplayTypes.generated.h"

UENUM(BlueprintType)
enum class ETeam : uint8
{
	White			UMETA(DisplayName = "WhiteTeam"),
	Black			UMETA(DisplayName = "BlackTeam")
};

UENUM(BlueprintType)
enum class EEndGameType : uint8
{
	FullGame			UMETA(DisplayName = "Full Game"),
	NumberOfPoints		UMETA(DisplayName = "Number Of Points")
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	Rifle 			UMETA(DisplayName = "Rifle"),
	Pistol			UMETA(DisplayName = "Pistol"),
	SubmachineGun	UMETA(DisplayName = "SubmachineGun"),
	Shotgun			UMETA(DisplayName = "Shotgun"),
	SniperRifle		UMETA(DisplayName = "SniperRifle"),
	Melee			UMETA(DisplayName = "Melee"),
	Special			UMETA(DisplayName = "Special"),
	Grenade			UMETA(DisplayName = "Grenade")
};

UENUM(BlueprintType)
enum class ESlotType : uint8
{
	Basic			UMETA(DisplayName = "Basic"),
	Secondary		UMETA(DisplayName = "Secondary"),
	Additional		UMETA(DisplayName = "Additional"),
	Belt			UMETA(DisplayName = "Belt"),
	None			UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum class EGameState : uint8
{
	Wait 		UMETA(DisplayName = "Wait"),
	Game			UMETA(DisplayName = "Game"),
	GameEnd		UMETA(DisplayName = "GameEnd")
};

//USTRUCT(BlueprintType)
//struct FDamageTimeMarker
//{
//	GENERATED_USTRUCT_BODY()
//
//	FDamageTimeMarker(float DamageTakingTime, float Damage)
//	{
//		DamageTakingTime = DamageTakingTime;
//		Damage = Damage;
//	}
//	FDamageTimeMarker(){}
//
//	UPROPERTY(BlueprintReadOnly)
//	float DamageTakingTime;
//
//	UPROPERTY(BlueprintReadOnly)
//	float Damage;
//};
//
//USTRUCT(BlueprintType)
//struct FDamageHistoryController
//{
//	GENERATED_USTRUCT_BODY()
//
//	float StoragedTime = 120;
//
//	UPROPERTY(BlueprintReadOnly)
//	TArray<FDamageTimeMarker> ListDamageReceived;
//
//	void AddDamageMarker(float DamageTakingTime, float Damage)
//	{
//		FDamageTimeMarker NewMarker(DamageTakingTime, Damage);
//		ListDamageReceived.Add(NewMarker);
//	}
//
//	float GetAmountDamageReceived(float PeriodInSeconds, float CurrentTime)
//	{
//		float DamageReceived = 0;
//
//		for (FDamageTimeMarker It : ListDamageReceived)
//		{
//			if (CurrentTime - It.DamageTakingTime < PeriodInSeconds)
//			{
//				DamageReceived += It.Damage;
//			}
//		}
//
//		//ClearOldEntires(CurrentTime); // Clear
//
//		return DamageReceived;
//	}
//
//	float GetTimeWithoutDamage(float CurrentTime)
//	{
//		return CurrentTime - ListDamageReceived[ListDamageReceived.Num() - 1].DamageTakingTime;
//	}
//
//	void ClearOldEntires(float CurrentTime)
//	{
//		for (int i = 0; i < ListDamageReceived.Num(); i++)
//		{
//			if (CurrentTime - ListDamageReceived[i].DamageTakingTime > StoragedTime)
//			{
//				ListDamageReceived.RemoveAt(i);
//			}
//		}
//	}
//};

USTRUCT(BlueprintType)
struct FSGameMap : public FTableRowBase
{
	GENERATED_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		FString RealMapName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		FText DisplayMapName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UTexture2D* MapIcon;
};

USTRUCT(BlueprintType)
struct FImpactParticleSet
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VisualSettings")
	UParticleSystem* ImpactConcrete;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VisualSettings")
	UParticleSystem* ImpactMetal;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VisualSettings")
	UParticleSystem* ImpactGlass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VisualSettings")
	UParticleSystem* ImpactWood;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VisualSettings")
	UParticleSystem* ImpactMeat;
};

//USTRUCT(BlueprintType)
//struct FSGameMapStyle
//{
//	GENERATED_UCLASS_BODY();
//
//	UPROPERTY(EditAnywhere)
//		FName StyledMapName;
//
//	UPROPERTY(EditAnywhere)
//		FText StyleName;
//
//	UPROPERTY(EditAnywhere)
//		UTexture2D* StyleIcon;
//};
