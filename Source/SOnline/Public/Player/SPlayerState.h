// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "SPlayerState.generated.h"

class ASOnlineCharacter;

UCLASS()
class SONLINE_API ASPlayerState : public APlayerState
{
	GENERATED_BODY()

protected:

	virtual void BeginPlay() override;

public:

	ASPlayerState();

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "PlayerStatistics")
	int Kills;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "PlayerStatistics")
	int Deaths;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "PlayerStatistics")
	bool Alive;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "PlayerInfo")
	bool ReadyForGame;

	UFUNCTION(NetMulticast, Reliable)
	void SetAlivePlayer(bool NewAlive);

	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation, Category = "PlayerInfo")
	void SetReadyForGame(bool ReadyStatus);

	void AddKill(int Killed);

	void AddDeath();

};
