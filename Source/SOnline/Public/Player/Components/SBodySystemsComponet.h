// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SGameplayTypes.h"
#include "SBodySystemsComponet.generated.h"

#define BODYSYSTEMS_DEBUG

class ASOnlineCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SONLINE_API USBodySystemsComponet : public UActorComponent
{
	GENERATED_BODY()

protected:

	UPROPERTY(BlueprintReadOnly)
	ASOnlineCharacter* OwnerComponent;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "BodyStats")
	bool IsBodyOff;

	///// ENERGY CHARACTERISTICS /////

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "BodyStats|Energy")
	float Energy;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BodySettings|Energy")
	float MaxEnergy;

	FTimerHandle EnergyUpdateTimer;

	///// SHIELD CHARACTERISTICS /////

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "BodyStats|Shield")
	bool EnableShieldSystem;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "BodyStats|Shield")
	float ShieldEffectivity;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BodySettings|Shield")
	float ShieldAbsorptionEfficiency;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BodySettings|Shield")
	float ShieldRegenerationSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BodySettings|Shield")
	float ShieldMaxEnergyDecrease;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BodySettings|Shield")
	float ShieldMinEnergyDecrease;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BodySettings|Shield")
	float TimeWithoutDamageForRegen;

	AActor* LastDamageCauser;

	float LastDamageTime;

	///// TEMPERATURE CHARACTERISTICS /////

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "BodyStats|Cooling")
	bool EnableCoolingSystem;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "BodyStats|Cooling")
	float CurrentTemperature;

	UPROPERTY(BlueprintReadOnly, Category = "BodyStats|Cooling")
	float PowerCoolingSysytem;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BodySettings|Cooling")
	float MaxCoolingTemp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BodySettings|Cooling")
	float CoolingSysMaxEnergyDecrease;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BodySettings|Cooling")
	float DefaultTemperatureRise;

	///// SPEEDED CHARACTERISTICS /////

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BodySettings")
	float WalkSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BodySettings")
	float SprintSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BodySettings|Energy")
	float EnergyDecreaseInSprint;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "BodyStats")
	bool IsSprint;

	UPROPERTY(BlueprintReadOnly, Category = "BodyStats")
	float SystemVoltage; // From 0 to 100

	virtual void BeginPlay() override;

	UFUNCTION()
	void ApplyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION()
	void BodySystemsTick(); // �alled 10 times per second

	void EnergyLogicTick(); // �alled in Body Systems Tick

	void TemperatureLogicTick(); // �alled in Body Systems Tick

	void ShieldLogicTick(); // �alled in Body Systems Tick

	UFUNCTION(Server, Reliable, WithValidation)
	void SwitchShieldSystemToServer();

	UFUNCTION(Server, Reliable, WithValidation)
	void SwitchCoolingSystemToServer();

	void DamageModules(float Damage);

	void EndOfEnergy();

	void BodyShutdown();

public:

	bool SwitchSprint(bool Start_Stop);

	bool SwitchShieldSystem();

	bool SwitchCoolingSystem();

	USBodySystemsComponet();

	///// GETTERS /////
	
};
