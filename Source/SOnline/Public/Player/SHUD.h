// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SOnlineTypes.h"
#include "SHUD.generated.h"

class UUserWidget;

UCLASS()
class SONLINE_API ASHUD : public AHUD
{
	GENERATED_BODY()

	virtual void BeginPlay() override;

	UFUNCTION()
	void OnChangeGameState(EGameState NewState);

	void ToogleHUD(bool Show);

	void TooglePreStart(bool Show);

protected:

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> HUD_WidgetClass;

	UUserWidget* HUD_Widget;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> PreStart_WidgetClass;

	UUserWidget* PreStart_Widget;
};
