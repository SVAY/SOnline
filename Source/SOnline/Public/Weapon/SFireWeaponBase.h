// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/SWeaponBase.h"
#include "SGameplayTypes.h"
#include "SFireWeaponBase.generated.h"



UCLASS()
class SONLINE_API ASFireWeaponBase : public ASWeaponBase
{
	GENERATED_BODY()
	
public:

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "WeaponStats")
	int CurrentAmmoInClip;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "WeaponStats")
	int TotalAmmo;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "WeaponStats")
	bool Reloading;

	ASFireWeaponBase();

	void Reload();

	void ReloadComplete();

	virtual void ChangeWeaponAction() override;

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponSettings")
	int MaxAmmoInClip;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponSettings")
	float ReloadTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponSettings")
	float RecoilForce;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponSettings")
	float FireDistance;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponSettings")
	float FireStartDistance;

	FTimerHandle ReloadTimer;

	///// VISUAL SET /////

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "VisualSettings")
	FName FireFlashSocketName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VisualSettings")
	UParticleSystem* FireFlashParticle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VisualSettings")
	UParticleSystem* TrailParticle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VisualSettings")
	FImpactParticleSet ImpactParticles;

	///// THE LOGIC OF THE WEAPON /////

	virtual void UseWeaponLogic() override;

	virtual void ServerShotTrace();

	virtual void ClientShotTrace();

	///// COSMETIC EFFECTS FUNCTION /////

	void PlayEffects(FVector TraceTo, FRotator RotationNormal, EPhysicalSurface SurfaceType, bool IsImpact);
};
