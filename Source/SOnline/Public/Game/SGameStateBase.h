// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Game/SOnlineGameState.h"
#include "SGameStateBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnPlayerDead, APlayerState*, DeadPlayer, APlayerState*, DeathInstigator, AActor*, DeathCauser);

UCLASS()
class SONLINE_API ASGameStateBase : public ASOnlineGameState
{
	GENERATED_BODY()
	
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameTime")
		int TimeWaitingBeforeStart;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "GameTime")
		int TimeWaitingAfterEnd;

	bool LimitByTimeFromGameMode;

	virtual void BeginPlay() override;

	virtual void GameStateTick() override;

	virtual void GameLogicTick() override;

	void SetTime_LimitFromGameMode();
	void SetTime_TimeWaitAfterEnd();

public:

	ASGameStateBase();

	UPROPERTY(Replicated, BlueprintReadOnly)
	int GameTime;

	UPROPERTY(BlueprintAssignable)
	FOnPlayerDead OnPlayerDead;

	UFUNCTION(NetMulticast, Reliable)
	void NotificationPlayerDead(APlayerState* DeadCharacter, APlayerState* DeathInstigator, AActor* DeathCausedBy);
};
