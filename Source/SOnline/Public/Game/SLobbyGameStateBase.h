// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SOnlineGameState.h"
#include "SOnlineTypes.h"
#include "SLobbyGameStateBase.generated.h"

class UDataTable;

USTRUCT(BlueprintType)
struct FVote
{
	GENERATED_USTRUCT_BODY()

	int MapIndex;

	APlayerController* Player;
};

UCLASS()
class SONLINE_API ASLobbyGameStateBase : public ASOnlineGameState
{
	GENERATED_BODY()

protected:

		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "LobbySettings")
		int PlayersForStartGame;

		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "LobbySettings")
		UDataTable* TableMapList;

		TArray<FVote> Votes;

		virtual void BeginPlay() override;

		virtual void GameStateTick() override; // Called every second

		virtual void PlayerReady(APlayerState* PlayerState) override;

		void StartGame();

		void DestroyLobby(FString Reason);

		bool ExtractMapFromTable();

		void UpdateMapsVotes();

		FSGameMap GetFavoriteMap();

		bool EnoughPlayersToStart();

public:

	UPROPERTY(BlueprintReadOnly, Category = "Lobby")
		TArray<FSGameMap> MapList;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Vote")
		TArray<int> VotesForMap;

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "LobbySettings") // Time waiting for players after which the lobby is destroyed
		int TimeToStart;

	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation)
	void SendVote(APlayerController* Player, int MapIndex);

	ASLobbyGameStateBase();
};
