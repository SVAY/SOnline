// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SOnlineTypes.h"
#include "SOnlineGameMode.generated.h"

class ASPlayerController;

UCLASS()
class SONLINE_API ASOnlineGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:

	virtual void PostLogin(APlayerController * NewPlayer) override;

	virtual void OnPlayerConnected(APlayerController* Controller);

public:

	ASOnlineGameMode();
};



